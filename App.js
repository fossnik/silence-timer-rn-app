import React from 'react';
import {View, Text, ImageBackground, StyleSheet, Button, Image} from 'react-native';

import DurationSelector from './src/components/duration_selector';

export default class App extends React.Component {
	state = {
		durationSeconds: 10 * 60,
		elapsedSeconds: 0,
	};

	timer = null;

	onDurationChange = value => {
		this.setState({
			durationSeconds: parseInt(value) * 60,
		});
	};

	onTimerStart = () => {
		clearInterval(this.timer);

		this.timer = setInterval(() => {
			if (this.state.elapsedSeconds >= this.state.durationSeconds) {
				this.timerIsDone();
			} else {
				this.setState({
					elapsedSeconds: this.state.elapsedSeconds + 1,
				});
			}
		}, 1000);
	};

	timerIsDone() {
		clearInterval(this.timer);
	}

	render = () => (
		<ImageBackground
			style={styles.imageBackground}
			source={require('./src/img/buddha.jpg')}
		>
			<View style={styles.mainContainer}>
				<Text style={styles.header}>Zen Timer</Text>
				<View style={styles.sectionBody}>
					<DurationSelector
						duration={this.state.durationSeconds}
						onDurationChange={this.onDurationChange}
					/>
					<View style={{
						alignItems: 'center',
						alignContent: 'center',
						backgroundColor: 'rgba(52, 52, 52, 0.8)',
					}}>
						<Text style={styles.text}>Elapsed Seconds</Text>
						<Text style={styles.text}>{this.state.elapsedSeconds}</Text>
					</View>
				</View>
				<Button
					title='Start'
					onPress={this.onTimerStart}
				>
				</Button>
				{
					this.state.elapsedSeconds >= this.state.durationSeconds ? (
						<Image
							style={styles.gongImage}
							source={require('./src/img/gong-1024x1024.jpg')}
						/>
					) : null
				}
			</View>
		</ImageBackground>
	);
}

const styles = StyleSheet.create({
	mainContainer: {
		flex: 1,
		alignItems: 'center',
		color: 'blue',
	},
	imageBackground: {
		flex: 1,
	},
	sectionTitle: {
		fontSize: 34,
		marginTop: 10,
		fontWeight: '600',
		color: 'white',
	},
	header: {
		fontSize: 80,
		color: 'blue',
	},
	sectionBody: {
		padding: 2,
	},
	startButton: {
		width: 50,
		backgroundColor: '#fff',
	},
	text: {
		textAlign: 'center',
		fontSize: 20,
		color: '#cecece',
	},
	gongImage: {
		width: '95%',
		resizeMode: 'contain',
		position: 'absolute',
	},
});
