import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Slider from '@react-native-community/slider';

const DurationSelector = (props) => (
	<View style={styles.durationSelector}>
		<Text style={styles.textHeader}>Duration</Text>
		<Text style={styles.text}>{props.duration / 60} min</Text>
		<Slider
			style={styles.slider}
			value={props.duration / 60}
			maximumValue={30}
			minimumValue={5}
			step={5}
			onValueChange={props.onDurationChange}
		/>
	</View>
);

const styles = StyleSheet.create({
	durationSelector: {
		width: '80%',
		backgroundColor: 'rgba(52, 52, 52, 0.8)',
		color: 'blue',
	},
	slider: {
		width: 300,
	},
	text: {
		textAlign: 'center',
		fontSize: 20,
		color: '#cecece',
	},
	textHeader: {
		textAlign: 'center',
		fontSize: 24,
		color: '#e9b3ab',
	},
});

export default DurationSelector;
